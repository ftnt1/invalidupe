# README #

FortiOS invalid duplicate objects finder (same names different values)

## Files ##

### invalidupe.py ###

This script prints all objects that have the same names but different values within one or more FortiOS config files/VDOM's. It skips UUID's. It is to prevent conflicts when importing to FMG

Run it from within the directory containing firewall config(s).

It takes no command line arguments

### README.md ###

This file