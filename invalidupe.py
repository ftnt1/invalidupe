#!/usr/bin/env python3

"""invalidupe.py: FortiOS invalid duplicates finder"""

__author__ = "Lukasz Korbasiewicz"
__maintainer__ = "Lukasz Korbasiewicz"
__email__ = "lkorbasiewicz@fortinet.com"
__version__ = "1.1"
__status__ = "Production"

import os

def uniq(lst):
    last = object()
    for item in lst:
        if item == last:
            continue
        yield item
        last = item


def sort_and_deduplicate(l):
    return list(uniq(sorted(l)))


def printnumlines(lst, s=0, start=0, end=None):
    for n, item in enumerate(lst):
        print(str(n+s) + ' - ' + str(item[start:end]))


BLUE ='\033[94m'
GREEN = '\033[92m'
YELLOW = '\033[93m'
RED = '\033[91m'
NORM = '\033[0m'

# Clear the terminal
os.system('cls' if os.name == 'nt' else 'clear')


# Open path
path = "./"
dirs = os.listdir(path)

configs = []
startlines = []
endlines = []
vdomindex = []
objects = []
obstart = []
obend = []
obs = []
for file in dirs:
    if ".conf" in file:
        print(str(file))
        f = open(file)
        cfg = f.readlines()
        f.close()
        for line in cfg:
            configs.append(line)
configs = [line for line in configs if 'set uuid' not in line]
print('')
for n, line in enumerate(configs):
    if line.endswith('config firewall address\n'):
        startlines.append(n)
    if line.endswith('config vdom\n'):
        vdomindex.append(n)
for index in startlines:
    for n, line in enumerate(configs):
        if line == 'end\n' and n > index:
            endlines.append(n)
            break

for q, s in enumerate(startlines):
    for line in configs[s:endlines[q]+1]:
        objects.append(line)

for x, line in enumerate(objects):
    if 'edit "' in line:
        obstart.append(x)
    if "next\n" in line:
        obend.append(x)
for index, x in enumerate(obstart):
    obs.append(objects[x:obend[index]+1])
obs = sort_and_deduplicate(obs)
dupelist = []
for l1 in obs:
    for l2 in obs:
        if l2[0] == l1[0]:
            if l2 != l1:
                dupelist.append(l1)
                dupelist.append(l2)
dupelist = sort_and_deduplicate(dupelist)
dupelist = [[line.strip('\n').strip(' ') for line in dup] for dup in dupelist]
for line in dupelist:
    line = str(line[0:-1])
    print(line.strip('[').strip(']').replace("'","").replace('edit "','"').replace(',',':    ', 1))